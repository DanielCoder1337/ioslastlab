//
//  ExperienceDetailViewController.swift
//  lab2
//
//  Created by user161035 on 12/8/19.
//  Copyright © 2019 daniel ekeroth. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var image: UIImageView!
    @IBOutlet var text: UITextView!
    @IBOutlet var period: UILabel!
    @IBOutlet var name: UILabel!
    var cellData: DataObject!
    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = UIImage(systemName: cellData.imageName)
        text.text = cellData?.text
        period.text = cellData?.timeInterval
        name.text = cellData?.name
        navigationBar.topItem!.title = cellData?.name
    }
    

}
