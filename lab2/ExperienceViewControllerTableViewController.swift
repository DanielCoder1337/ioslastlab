//
//  ExperienceViewControllerTableViewController.swift
//  lab2
//
//  Created by user161035 on 12/8/19.
//  Copyright © 2019 daniel ekeroth. All rights reserved.
//

import UIKit

struct DataObject{
    var imageName: String
    var timeInterval: String
    var name: String
    var text: String
}



class ExperienceViewControllerTableViewController: UITableViewController {
    var dataSourceArray: [[DataObject]] = []
    let sections: [String] = ["Work", "Education"]
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDataSource()
    }
    
    func loadDataSource(){
        var work: [DataObject] = []
        var education: [DataObject] = []
        work.append(DataObject(imageName: "gamecontroller.fill", timeInterval: "2013-2014", name: "Work 1", text: "Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1" ))
        work.append(DataObject(imageName: "car.fill", timeInterval: "2014-2017", name: "Work 2", text: "Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1"))
        work.append(DataObject(imageName: "a", timeInterval: "2017-2018", name: "Work 3", text: "Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1"))
        education.append(DataObject(imageName: "book", timeInterval: "2018-current", name: "Education", text: "Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1Work 1"))
        dataSourceArray.append(work)
        dataSourceArray.append(education)
    }

    // MARK: - Table view data source

    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "experienceDetail", sender: dataSourceArray[indexPath.section][indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            guard let destinationVC = segue.destination as? ExperienceDetailViewController else {return}
            let selectedRow = indexPath.row
            let selectedSection = indexPath.section
            destinationVC.cellData = dataSourceArray[selectedSection][selectedRow]
        }
}
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceArray.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray[section].count
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < sections.count {
            return sections[section]
        }

        return nil
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = dataSourceArray[indexPath.section][indexPath.row]

        let cellC = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CellControllerTableViewCell
        
        cellC?.workName.text = cellData.name
        let image = UIImage(systemName: cellData.imageName)
        cellC?.imageNameFile!.image = image
        cellC?.timeInterval.text = cellData.timeInterval
        return cellC!
    }
    

}
