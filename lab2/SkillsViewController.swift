//
//  SkillsViewController.swift
//  lab2
//
//  Created by user161035 on 12/8/19.
//  Copyright © 2019 daniel ekeroth. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {
    @IBOutlet var AnimatedUiView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1){
            self.AnimatedUiView.center.y -= 100
        }
    }
    

    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
